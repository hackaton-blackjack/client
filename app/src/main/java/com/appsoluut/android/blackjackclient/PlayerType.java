package com.appsoluut.android.blackjackclient;

/**
 * Created by a.van.loo on 13/03/2017.
 */

public enum PlayerType {
    DEALER(R.id.dealer),
    PLAYER(R.id.player);

    private final int value;
    private PlayerType(int value) {
        this.value = value;
    }

    public int getResourceId() {
        return value;
    }
}
