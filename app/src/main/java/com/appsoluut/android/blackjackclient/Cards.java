package com.appsoluut.android.blackjackclient;

import android.support.annotation.DrawableRes;

import java.util.ArrayList;
import java.util.List;

public class Cards {

    public enum Rank {
        Ace(1, 11), King(10), Queen(10), Jack(10), Nine(9),
        Eight(8), Seven(7), Six(6), Five(5), Four(4), Three(3),
        Deuce(2);

        private int[] value;

        Rank(int... scores) {
            value = scores;
        }

        public int getPoints() {
            return value[0];
        }
    }

    public enum Suit {
        Clubs(R.drawable.card_clubs), Diamond(R.drawable.card_diamonds), Hearts(R.drawable.card_hearts), Spades(R.drawable.card_spades);

        private int drawableId;

        Suit(@DrawableRes int drawableId) {
            this.drawableId = drawableId;
        }

        public int getDrawableId() {
            return drawableId;
        }
    }

    private final Rank rank;
    private final Suit suit;

    private Cards(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    private static final List<Cards> deck = new ArrayList<>();

    // Initialize deck
    static {
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                deck.add(new Cards(rank, suit));
            }
        }
    }

    public static ArrayList<Cards> newDeck() {
        return new ArrayList<>(deck);
    }
}
