package com.appsoluut.android.blackjackclient;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by a.van.loo on 10/03/2017.
 */

@SuppressLint("AppCompatCustomView")
public class BlackJackTextView extends TextView {
    public BlackJackTextView(Context context) {
        super(context);

    }

    public BlackJackTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlackJackTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);

        if ( ( (View) this.getParent().getParent().getParent()).getId() == PlayerType.DEALER.getResourceId()){
            ( (LinearLayout) this.getParent().getParent()).setRotation(180);
        }
    }
}
