package com.appsoluut.android.blackjackclient;

import java.net.PasswordAuthentication;

/**
 * Created by a.van.loo on 13/03/2017.
 */

public enum Advise {
    HIT,
    PASS,
    GOODLUCK,
    DEAD;

    public String getDisplayMessage() {
        switch (this){
            case HIT:
                return "Better pick";
            case GOODLUCK:
                break;
            case DEAD:
                return "Game over";
            case PASS:
                return "!STOP!";
        }
        return "You're own your own";
    }
}
