package com.appsoluut.android.blackjackclient;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final private String TAG = MainActivity.class.getSimpleName();

    private LinearLayout playerPlayground;
    private LinearLayout dealerPlayground;

    private List<Cards> deck;
    private List<Cards> player;
    private List<Cards> dealer;

    private GameMode mode = GameMode.Player;

    private Handler handler = new Handler(Looper.getMainLooper());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playerPlayground = (LinearLayout) findViewById(PlayerType.PLAYER.getResourceId()).findViewById(R.id.playground);
        dealerPlayground = (LinearLayout) findViewById(PlayerType.DEALER.getResourceId()).findViewById(R.id.playground);

        dealerPlayground.setRotation(180);

        findViewById(R.id.button_hit).setOnClickListener(this);
        findViewById(R.id.button_pass).setOnClickListener(this);
        findViewById(R.id.button_new_game).setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        newGame();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        findViewById(R.id.button_hit).setOnClickListener(null);
        findViewById(R.id.button_pass).setOnClickListener(null);
        findViewById(R.id.button_new_game).setOnClickListener(null);
    }

    private void newGame() {
        deck = Cards.newDeck();
        Collections.shuffle(deck);

        mode = GameMode.Player;

        player = new ArrayList<>();
        dealer = new ArrayList<>();

        playerPlayground.removeAllViews();
        dealerPlayground.removeAllViews();

        hit();
    }

    private void hit() {
        if (deck.size() == 0)
            return;

        Cards card = deck.remove(0);

        LinearLayout layout;
        if (mode == GameMode.Player) {
            layout = playerPlayground;
        } else {
            layout = dealerPlayground;
        }


        final String rank;
        Cards.Rank cardRank = card.getRank();

        final LinearLayout cardView = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.card, layout, false);
        switch (cardRank) {
            case Jack:
            case Queen:
            case King:
            case Ace: {
                rank = String.valueOf(cardRank.name().charAt(0));
                break;
            }
            default:
                rank = String.valueOf(cardRank.getPoints());
                break;
        }
        ((TextView) cardView.findViewById(R.id.value)).setText(rank);
        ((ImageView) cardView.findViewById(R.id.suit)).setImageDrawable(getResources().getDrawable(card.getSuit().getDrawableId()));
        layout.addView(cardView);

        if (mode == GameMode.Player) {
            player.add(card);
            predictNextStepPlayer();
        } else {
            dealer.add(card);
            final int dealerScore = resultOfHand(dealer);
            setScore(PlayerType.DEALER, dealerScore);
            runDealer();
        }

    }

    private void pass() {
        if (mode != GameMode.Dealer) {
            mode = GameMode.Dealer;
            hit();
        }
    }

    private void predictNextStepPlayer() {
        // Odds calculation here
        // Should we hit or should we pass :)
        //
        int handValue = resultOfHand(player);

        setScore(PlayerType.PLAYER, handValue);

        if (handValue > 21){
            setAdvise(PlayerType.PLAYER, Advise.DEAD);
            playerLose();
            return;
        }
        Advise ad = hitOrPass(player);
        setAdvise(PlayerType.PLAYER, ad);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_hit:
                hit();
                break;
            case R.id.button_pass:
                pass();
                break;
            case R.id.button_new_game:
                newGame();
                break;
        }
    }

    private void setScore(PlayerType playerType, int handValue) {

        LinearLayout playerBase = (LinearLayout) findViewById(playerType.getResourceId());
        TextView msg = (TextView) playerBase.findViewById(R.id.score);
        msg.setText( String.valueOf(handValue));
    }

    public void setAdvise(PlayerType playerType, Advise advise) {
        LinearLayout playerBase = (LinearLayout) findViewById(playerType.getResourceId());
        BlackJackTextView msg = (BlackJackTextView) playerBase.findViewById(R.id.bericht);
        msg.setText(advise.getDisplayMessage());
    }

    private void runDealer() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                final int playerScore = resultOfHand(player);
                final int dealerScore = resultOfHand(dealer);

                if (dealerScore < playerScore) {
                    hit();
                    return;
                }

                if (dealerScore >= playerScore && dealerScore <= 21) {
                    // Dealer wins
                    playerLose();
                } else {
                    playerWin();
                }
            }
        }, 1000);
    }

    private void playerWin() {
        startActivity(new Intent(this, YouWinActivity.class));
    }

    private void playerLose() {
        startActivity(new Intent(this, GameOverActivity.class));
    }

    public int resultOfHand(List<Cards> hand){
        int numberOfAces = 0;
        int totalValue = 0;
        Iterator<Cards> cards = hand.iterator();
        while (cards.hasNext()) {
            Cards c = cards.next();
            if(c.getRank() == Cards.Rank.Ace) {
                numberOfAces++;
            }else{
              totalValue += c.getRank().getPoints();
            }
        }
        // ace value
        //( {min-value, max-value}, ...}
        int[][] ace_value={ {0,0}, {1,11}, {2, 12}, {3, 13}, {4, 14} };
        int totalValueLow = totalValue + ace_value[numberOfAces][0];
        int totalValueMax = totalValue + ace_value[numberOfAces][1];

        //totalValue of the hand
        totalValue = totalValueLow;
        if (totalValueMax < 22) {
            totalValue = totalValueMax;
        }
        return totalValue;

        //System.out.println("numberOfAces:" + numberOfAces);
        //System.out.println("totalValue:" + totalValue);

    }

    public Advise hitOrPass(List<Cards> hand){
        int numberOfCardsInDeck = deck.size();
        int totalValueCurrentHand = resultOfHand(hand);

        int countOnPostiveResult = 0;

        //Calculate amount of potential good picks
        Iterator<Cards> cards = deck.iterator();
        while (cards.hasNext()) {
            List<Cards> potentialNewHand = new ArrayList<>(hand);
            Cards c = cards.next();
            potentialNewHand.add(c);

            int totalValueNewHand =  resultOfHand(potentialNewHand);
            if (totalValueNewHand < 22 && totalValueCurrentHand < totalValueNewHand ){
                countOnPostiveResult++;
            }
        }

        //Calculate percantage of good picks
        float res = (float) countOnPostiveResult /  (float) numberOfCardsInDeck;

        System.out.println("Calculate totalValueCurrentHand: " + totalValueCurrentHand);
        System.out.println("Calculate percantage of good picks:" + res + " (" + countOnPostiveResult + " " + numberOfCardsInDeck + ")");

        if (res < 0.45) return Advise.PASS;
        if (res > 0.45 && res < 0.55) return Advise.GOODLUCK;
        return Advise.HIT;

    }
}
