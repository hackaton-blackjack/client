package com.appsoluut.android.blackjackclient;

public enum GameMode {
    Player,
    Dealer
}
